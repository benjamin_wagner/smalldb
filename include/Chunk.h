#ifndef SMALLDB_CHUNK_H
#define SMALLDB_CHUNK_H

#include <cstdint>
#include <unordered_map>
#include <vector>

const size_t CHUNK_DEFAULT_SIZE = 4096;

/// A chunk of data being passed from one operator to the next.
struct Chunk {
public:
    /// The chunk capacity.
    size_t capacity;
    /// The size of the current chunk.
    size_t chunkSize;
    /// Map from column identifiers to the chunks.
    std::unordered_map<std::string, std::vector<uint64_t>> storage;

    Chunk(size_t capacity): capacity(capacity), chunkSize(), storage() {
    };

    /// Attach a new column to the chunk.
    void attach(const std::string& id) {
        if (storage.find(id) != storage.end()) {
            throw std::runtime_error("Duplicate column name in chunk");
        }
        storage.insert({id, std::vector<uint64_t>(capacity)});
    }

    /// Get a specific column from the chunk.
    std::vector<uint64_t>* getColumn(const std::string& colId) {
        auto res = storage.find(colId);
        if (res == storage.end()) {
            return nullptr;
        }
        return &(res->second);
    }
};

#endif //SMALLDB_CHUNK_H
