#ifndef SMALLDB_DRIVER_H
#define SMALLDB_DRIVER_H

#include <iostream>
#include "Operators.h"

using RowMap = std::unordered_map<std::string, uint64_t>;

/// Run an execution plan and apply a function on each row.
void runWithPrint(Operator& op) {
    size_t total = 0;
    op.open();
    Chunk* chunk = op.nextChunk();
    // Output sql header
    auto headerIt = chunk->storage.begin();
    for (size_t k = 0; k < chunk->storage.size() - 1; ++k) {
        std::cout << headerIt->first << ",";
        headerIt++;
    }
    std::cout << headerIt->first << "\n";

    do {
        for (uint64_t k = 0; k < chunk->chunkSize; ++k) {
            auto headerIt = chunk->storage.begin();
            for (size_t j = 0; j < chunk->storage.size() - 1; ++j) {
                std::cout << headerIt->second[k] << ",";
                headerIt++;
            }
            std::cout << headerIt->second[k] << "\n";
        }
        chunk = op.nextChunk();
    } while (chunk != nullptr);
    op.close();
}

/// Run an execution plan and return the result size.
size_t runWithCount(Operator& op) {
    size_t total = 0;
    op.open();
    while(Chunk* chunk = op.nextChunk()) {
        total += chunk->chunkSize;
    }
    op.close();
    return total;
}

#endif //SMALLDB_DRIVER_H
