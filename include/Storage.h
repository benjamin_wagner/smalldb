#ifndef SMALLDB_STORAGE_H
#define SMALLDB_STORAGE_H

#include <vector>
#include <unordered_map>
#include <string>
#include <stdexcept>

class RelationStorage {
private:
    std::string relName;
    std::unordered_map<std::string, std::vector<uint64_t>> columns;
    size_t size;

public:
    RelationStorage(std::string relName, size_t size): relName(std::move(relName)), columns(), size(size) {
    };

    const std::vector<uint64_t>* getColumn(const std::string& colName) const {
        auto res = columns.find(colName);
        if (res == columns.end()) {
            return nullptr;
        }
        return &(res->second);
    };

    std::vector<std::string> getColnames() {
        std::vector<std::string> res;
        for (const auto& col: columns) {
            res.emplace_back(col.first);
        }
        return res;
    }

    void addColumn(const std::string& colName, std::vector<uint64_t> data) {
        if (data.size() != size) {
            throw std::runtime_error("Column length does not match row count");
        }
        columns[colName] = std::move(data);
    }

    size_t getSize() const {
        return size;
    }
};

#endif //SMALLDB_STORAGE_H
