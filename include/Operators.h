#ifndef SMALLDB_OPERATORS_H
#define SMALLDB_OPERATORS_H

#include <memory>
#include <cassert>
#include "Storage.h"
#include "Chunk.h"

class Operator;
using OperatorPtr = std::unique_ptr<Operator>;

/// Abstract base operator class.
class Operator {
protected:
    std::vector<OperatorPtr> children;

    /// Helper function to open all children.
    void openChildren() {
        for (auto& child: children) {
            child->open();
        }
    }

    /// Helper function to close all children.
    void closeChildren() {
        for (auto& child: children) {
            child->close();
        }
    }

public:
    Operator(): children() {
    };
    virtual ~Operator() = default;

    /// Open the operator and do initialization work.
    virtual void open() {
        openChildren();
    };
    /// Get the next chunk of data from the operator. A nullptr indicates that there is no more data to be read.
    virtual Chunk* nextChunk() = 0;
    /// Close the operator and do termination work.
    virtual void close() {
        closeChildren();
    };
    /// Get a sample chunk containing the correct columns.
    virtual Chunk* sampleChunk() {
        return children[0]->sampleChunk();
    };
};

/// A table scan operator which reads data in chunks from a RelationStorage.
class TableScanOp final: public Operator {
private:
    /// The backing relation.
    RelationStorage& relation;
    /// The current chunk passing through the pipeline.
    std::unique_ptr<Chunk> chunk;
    /// The set of columns which we want to read from.
    std::vector<std::string> scanSet;
    /// The current cursor on the scan.
    size_t cursor;

public:

    TableScanOp(RelationStorage& relation, std::vector<std::string> scanSet): Operator(), relation(relation), chunk(), scanSet(std::move(scanSet)), cursor(0) {
    }

    void open() override {
        // Create the chunk.
        chunk = std::make_unique<Chunk>(CHUNK_DEFAULT_SIZE);
        // Setup the columns which we will read.
        for (const auto& colName: scanSet) {
            if (!relation.getColumn(colName)) {
                throw std::runtime_error("Relation column cannot be read by scan.");
            }
            chunk->attach(colName);
        }
        cursor = 0;
    }

    Chunk* nextChunk() override {
        if (cursor == relation.getSize()) {
            // We already scanned the whole relation.
            return nullptr;
        }
        // Scan the data from the underlying relation.
        size_t to = std::min(cursor + chunk->capacity, relation.getSize());
        for (const auto& column: scanSet) {
            const auto& relationCol = *relation.getColumn(column);
            auto& chunkCol = chunk->storage[column];
            size_t chunkIndex = 0;
            for (size_t k = cursor; k < to; ++k) {
                chunkCol[chunkIndex++] = relationCol[k];
            }
        }
        chunk->chunkSize = to - cursor;
        cursor = to;
        return chunk.get();
    }

    Chunk* sampleChunk() override {
        return chunk.get();
    }

    void close() override {
        chunk.reset();
    }
};

/// Different operators for filter operations.
enum class FilterOpcode {
    EqualTo,
    LessThan,
    GreaterThan,
};

inline bool filterEq(const uint64_t& op1, const uint64_t& op2) {
    return op1 == op2;
}

inline bool filterLT(const uint64_t& op1, const uint64_t& op2) {
    return op1 < op2;
}

inline bool filterGT(const uint64_t& op1, const uint64_t& op2) {
    return op1 > op2;
}

template <bool CMP(const uint64_t&, const uint64_t&)>
void runFilter(std::vector<size_t>& accept, const std::vector<uint64_t>& data, uint64_t cmp, size_t inputSize) {
    for (size_t k = 0; k < inputSize; ++k) {
        if (CMP(data[k], cmp)) {
            accept.push_back(k);
        }
    }
}

class FilterOp final: public Operator {
private:
    std::string colName;
    uint64_t constant;
    FilterOpcode opcode;

public:
    FilterOp(OperatorPtr child, std::string colName, uint64_t constant, FilterOpcode opcode):
        Operator(), colName(colName), constant(constant), opcode(opcode) {
        children.push_back(std::move(child));
    }

    void open() override {
        openChildren();
        auto sample = children[0]->sampleChunk();
        if (!sample->getColumn(colName)) {
            throw std::runtime_error("Filter child does not contain correct column.");
        }
    }

    Chunk* nextChunk() override {
        auto chunk = children[0]->nextChunk();
        if (chunk) {
            // Run the actual filter.
            std::vector<size_t> accept;
            accept.reserve(chunk->chunkSize);
            auto& col = *(chunk->getColumn(colName));
            switch (opcode) {
                case FilterOpcode::EqualTo: {
                    runFilter<filterEq>(accept, col, constant, chunk->chunkSize);
                    break;
                }
                case FilterOpcode::LessThan: {
                    runFilter<filterLT>(accept, col, constant, chunk->chunkSize);
                    break;
                }
                case FilterOpcode::GreaterThan: {
                    runFilter<filterGT>(accept, col, constant, chunk->chunkSize);
                    break;
                }
            }
            // Remove the tuples which didn't pass the filter.
            for (auto& entry: chunk->storage) {
                auto& column = entry.second;
                for (size_t index = 0; index < accept.size(); ++index) {
                    column[index] = column[accept[index]];
                }
            }
            chunk->chunkSize = accept.size();
        }
        return chunk;
    }
};

enum class AggregationOpcode {
    Count,
    Sum,
};

inline void aggCount(const uint64_t& val, uint64_t& state) {
    state += 1;
}

inline void aggSum(const uint64_t& val, uint64_t& state) {
    state += val;
}

template <void AGG(const uint64_t&, uint64_t&)>
void aggregateChunkGroup(std::unordered_map<uint64_t, uint64_t>& aggState, const std::vector<uint64_t>& aggCol, const std::vector<uint64_t>& groupCol, size_t size) {
    for (size_t k = 0; k < size; ++k) {
        AGG(aggCol[k], aggState[groupCol[k]]);
    }
}

template <void AGG(const uint64_t&, uint64_t&)>
void aggregateChunkNoGroup(std::unordered_map<uint64_t, uint64_t>& aggState, const std::vector<uint64_t>& aggCol, size_t size) {
    uint64_t& state = aggState[0];
    for (size_t k = 0; k < size; ++k) {
        AGG(aggCol[k], state);
    }
}

class AggregationOp final: public Operator {
private:
    /// The current chunk passing through the pipeline.
    std::unique_ptr<Chunk> chunk;
    /// The column over which we should group.
    std::optional<std::string> groupCol;
    /// The column over which we should aggregate.
    std::string aggregationCol;
    /// The name of the aggregated result column.
    std::string resultCol;
    /// Aggregation operation to be performed.
    AggregationOpcode opcode;
    /// Internal aggregation state.
    std::unordered_map<std::uint64_t, uint64_t> state;
    /// Result iterator when we are returning chunks.
    std::optional<decltype(state.begin())> resultIt;
    /// Was nextChunk not called on the operator so far?
    bool called = false;

public:
    AggregationOp(OperatorPtr child, std::optional<std::string> groupCol, std::string aggregationCol, std::string resultCol, AggregationOpcode opcode)
        : Operator(), groupCol(std::move(groupCol)), aggregationCol(std::move(aggregationCol)), resultCol(std::move(resultCol)), opcode(opcode), state(), resultIt(std::nullopt) {
        children.push_back(std::move(child));
    }

    void open() override {
        // Create the chunk.
        chunk = std::make_unique<Chunk>(CHUNK_DEFAULT_SIZE);
        if (groupCol.has_value())
            chunk->attach(*groupCol);
        chunk->attach(resultCol);
        called = false;

        // Open the children.
        openChildren();

        // Do a quick semantic check.
        auto sample = children[0]->sampleChunk();
        if (!sample->getColumn(aggregationCol)) {
            throw std::runtime_error("Aggregation child does not contain aggregation column.");
        }
        if (groupCol.has_value() && !sample->getColumn(*groupCol)) {
            throw std::runtime_error("Aggregation child does not contain aggregation column.");
        }
    }

    Chunk* nextChunk() override {
        if (!called) {
            // Consume all input chunks.
            called = true;
            while (Chunk* inputChunk= children[0]->nextChunk()) {
                auto& colAgg = *inputChunk->getColumn(aggregationCol);
                if (groupCol.has_value()) {
                    auto& colGroup = *inputChunk->getColumn(*groupCol);
                    if (opcode == AggregationOpcode::Count) {
                        aggregateChunkGroup<aggCount>(state, colAgg, colGroup, inputChunk->chunkSize);
                    } else {
                        aggregateChunkGroup<aggSum>(state, colAgg, colGroup, inputChunk->chunkSize);
                    }
                } else {
                    if (opcode == AggregationOpcode::Count) {
                        aggregateChunkNoGroup<aggCount>(state, colAgg, inputChunk->chunkSize);
                    } else {
                        aggregateChunkNoGroup<aggSum>(state, colAgg, inputChunk->chunkSize);
                    }
                }
            }
            resultIt = state.begin();
        }
        if (resultIt == state.end()) {
            return nullptr;
        }
        // Materialize output.
        auto& resultAggCol = *chunk->getColumn(resultCol);
        if (groupCol.has_value()) {
            auto& resultGroupCol = *chunk->getColumn(*groupCol);
            size_t k = 0;
            while (k < chunk->capacity && *resultIt != state.end()) {
                resultGroupCol[k] = (*resultIt)->first;
                resultAggCol[k] = (*resultIt)->second;
                (*resultIt)++;
                k++;
            }
            chunk->chunkSize = k;
        } else {
            // We return just one tuple.
            resultAggCol[0] = (*resultIt)->second;
            (*resultIt)++;
            chunk->chunkSize = 1;
        }
        return chunk.get();
    }

};

class HashJoinOp final: public Operator {
private:
    // Mapping from keys to tuple indexes.
    using StateMap = std::unordered_multimap<uint64_t, size_t>;
    StateMap hashMap;
    std::string colLeft;
    std::string colRight;
    std::unique_ptr<Chunk> chunk;
    std::unordered_map<std::string, std::vector<uint64_t>> storage;
    bool called = false;
    bool done = true;

    struct LookupState {
        Chunk* pendingChunk;
        uint64_t currentIndex;
        std::pair<StateMap::iterator, StateMap::iterator> keyIterator;
    };
    std::optional<LookupState> lookupState;

    void obtainMatch() {
        // Obtain the proper iterator for the current right join partner.
        auto& joinCol = *lookupState->pendingChunk->getColumn(colRight);
        lookupState->keyIterator = hashMap.equal_range(joinCol[lookupState->currentIndex]);
    }

    bool advanceInternalState() {
        while (true) {
            // Try to move to the next join key.
            if (lookupState->keyIterator.first != lookupState->keyIterator.second) {
                // We found a join partner.
                return true;
            }
            else if (lookupState->currentIndex + 1 < lookupState->pendingChunk->chunkSize) {
                // Try to move to the next matching tuple on the left side.
                lookupState->currentIndex++;
            } else {
                // Move on to the next chunk.
                lookupState->pendingChunk = children[1]->nextChunk();
                if (!lookupState->pendingChunk) {
                    return false;
                }
                lookupState->currentIndex = 0;
            }
            obtainMatch();
        }
    }

    bool initializeInternalState() {
        lookupState = LookupState {};
        // Get the first chunk from the right side.
        lookupState->pendingChunk = children[1]->nextChunk();
        if (!lookupState->pendingChunk) {
            return false;
        }
        lookupState->currentIndex = 0;
        obtainMatch();
        if (lookupState->keyIterator.first == lookupState->keyIterator.second) {
            return advanceInternalState();
        }
        return true;
    }


public:
    HashJoinOp(OperatorPtr childLeft, OperatorPtr childRight, std::string colLeft, std::string colRight)
    : Operator(), hashMap(), colLeft(std::move(colLeft)), colRight(std::move(colRight)), chunk() {
        children.push_back(std::move(childLeft));
        children.push_back(std::move(childRight));
    }

    void open() override {
        // Create the chunk.
        chunk = std::make_unique<Chunk>(CHUNK_DEFAULT_SIZE);
        called = false;
        done = false;
        lookupState = std::nullopt;

        // Open the children.
        openChildren();

        // Do a quick semantic check and add result columns.
        auto sampleL = children[0]->sampleChunk();
        if (!sampleL->getColumn(colLeft)) {
            throw std::runtime_error("HashJoin child does not contain left join column.");
        }
        auto sampleR = children[1]->sampleChunk();
        if (!sampleR->getColumn(colRight)) {
            throw std::runtime_error("HashJoin child does not contain right join column.");
        }
        for (auto& elem: sampleL->storage) {
            chunk->attach(elem.first);
            storage.insert({elem.first, {}});
        }
        for (auto& elem: sampleR->storage) {
            chunk->attach(elem.first);
        }
    }

    Chunk* nextChunk() override {
        if (!called) {
            called = true;
            // Materialize all input tuples.
            while (Chunk* chunkL = children[0]->nextChunk()) {
                size_t chunkSize = chunkL->chunkSize;
                size_t storageIndex = storage.begin()->second.size();
                // Add the join keys to the hash map.
                auto joinCol = *chunkL->getColumn(colLeft);
                for (size_t k = 0; k < chunkSize; ++k) {
                    hashMap.insert({joinCol[k], storageIndex + k});
                }
                // Add all columns to the backing storage:
                for (auto& col: chunkL->storage) {
                    auto& vecTarget = storage[col.first];
                    auto& vecSrc = chunkL->storage[col.first];
                    vecTarget.reserve(vecTarget.size() + chunkSize);
                    for (size_t k = 0; k < chunkSize; ++k) {
                        vecTarget.push_back(vecSrc[k]);
                    }
                }

            }
            // Initialize lookup state.
            if (!initializeInternalState()) {
                return nullptr;
            }
        }
        if (done) {
            // We are already done.
            return nullptr;
        }
        // Try to fill up the current chunk.
        size_t current = 0;
        while (current < chunk->capacity) {

            size_t buildIndex = lookupState->keyIterator.first->second;
            size_t probeIndex = lookupState->currentIndex;
            // Materialize the right side of the result tuple.
            for (const auto& elem: lookupState->pendingChunk->storage) {
                chunk->storage[elem.first][current] = elem.second[probeIndex];
            }
            // Materialize the left side of the result tuple.
            for (const auto& elem: storage) {
                chunk->storage[elem.first][current] = storage[elem.first][buildIndex];
            }

            current++;
            lookupState->keyIterator.first++;

            if (!advanceInternalState()) {
                // Can't get any more matches from the right side, return.
                done = true;
                break;
            }
        }
        chunk->chunkSize = current;
        return chunk.get();
    }
};

#endif //SMALLDB_OPERATORS_H
