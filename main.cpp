#include <iostream>
#include <random>
#include "include/Operators.h"
#include "include/Driver.h"

std::vector<uint64_t> randomColumn(uint64_t min, uint64_t max, size_t size, size_t seed = 42) {
    std::vector<uint64_t> result(size);
    // Fill with uniform data.
    std::mt19937 gen(seed);
    std::uniform_int_distribution<uint64_t> distrib(min, max);
    for (size_t k = 0; k < size; ++k) {
        result[k] = distrib(gen);
    }
    return result;
}

std::vector<uint64_t> increasingColumn(uint64_t start, size_t size) {
    std::vector<uint64_t> result(size);
    // Fill with increasing data.
    for (uint64_t k = 0; k < size; ++k) {
        result[k] = k + start;
    }
    return result;
}

std::vector<uint64_t> constantColumn(uint64_t val, uint64_t size) {
    std::vector<uint64_t> result(size);
    for (size_t k = 0; k < size; ++k) {
        result[k] = val;
    }
    return result;
}

int main() {
    const uint64_t tablesize = 1000;
    // First relation with tablesize tuples.
    RelationStorage relation("Rel1", tablesize);
    // Add key column
    std::string keyCol = "key";
    relation.addColumn(keyCol, increasingColumn(0, tablesize));
    // Add two non-key random columns.
    std::string valCol1 = "val1";
    std::string valCol2 = "val2";
    relation.addColumn(valCol1, randomColumn(0, 10, tablesize, 42));
    relation.addColumn(valCol2, randomColumn(0, 100, tablesize, 42));

    RelationStorage relationSimple1("Rel1", tablesize);
    std::string simple1Col1 = "1col1";
    relationSimple1.addColumn(simple1Col1, increasingColumn(0, tablesize));
    std::string simple1Col2 = "1col2";
    relationSimple1.addColumn(simple1Col2, randomColumn(0, 10, tablesize, 42));
    RelationStorage relationSimple2("Rel2", tablesize);
    std::string simple2Col1 = "2col1";
    relationSimple2.addColumn(simple2Col1, increasingColumn(0, tablesize));
    std::string simple2Col2 = "2col2";
    relationSimple2.addColumn(simple2Col2, randomColumn(0, 10, tablesize, 43));

    // Example query 1:
    /*
    auto root = std::make_unique<FilterOp>(
            std::make_unique<TableScanOp>(relation, std::vector<std::string>{keyCol, valCol2}),
            valCol2, 5, FilterOpcode::EqualTo);
    auto root = std::make_unique<AggregationOp>(
            std::make_unique<FilterOp>(
                std::make_unique<TableScanOp>(relation, std::vector<std::string>{keyCol, valCol1, valCol2}),
                valCol1, 6, FilterOpcode::LessThan), valCol1, valCol2, "sum", AggregationOpcode::Sum);
    */
    auto root = std::make_unique<HashJoinOp>(
            std::make_unique<TableScanOp>(relationSimple1, std::vector<std::string>{simple1Col1, simple1Col2}),
            std::make_unique<TableScanOp>(relationSimple2, std::vector<std::string>{simple2Col1, simple2Col2}),
            simple1Col1, simple2Col1
            );

    runWithPrint(*root);
    // std::cout << runWithCount(*root) << "\n";

    return 0;
}
